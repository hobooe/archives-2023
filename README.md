# Repository Description

This repository hosts the verifier archives for
the International Competition on Software Verification (SV-COMP).

# Contributing

Participants of SV-COMP can file a merge request to this repository in order
to make their verifier available for the competition execution,
to the SV-COMP jury for inspection, and for later replication of the results.

For SV-COMP 2023, the archives are stored as
`2023/<verifier>.zip`, where `<verifier>` is
the identifier for the verifier,
i.e., there exists a file `<verifier>.xml`
in the repository with the benchmark definitions:
https://gitlab.com/sosy-lab/sv-comp/bench-defs/tree/main/benchmark-defs

Validators use a prefix `val_`, and can be sym-linked to a verifier archive.

By filing a pull/merge request to this repository, contributors confirm
that the license for the archive is compatible with
the requirements of SV-COMP, as outlined in the rules
https://sv-comp.sosy-lab.org/2022/rules_2021-11-25.pdf
under section Competition Environment and Requirements / Verifier.

## Sparse checkout

Due to the large size of this repository, adding/updating a single verifier
is most convenient using _sparse checkout_, which does not require
downloading and storing all other tools' archives.

Assuming you have Developer access to this repository (ask the maintainer Dirk),
follow these steps:

1. Partially clone repository: `git clone --filter=blob:none --no-checkout git@gitlab.com:sosy-lab/sv-comp/archives-2023.git`
2. Navigate to repository: `cd archives-2023`
3. Configure sparse checkout: `git sparse-checkout set --no-cone /2023/<verifier>.zip`, where `<verifier>` is the identifier for the verifier
4. Create new branch: `git switch -c <branch>`, where `<branch>` is a relevant branch name
5. Perform actual checkout: `git checkout`
6. Add/update `2023/<verifier>.zip`
7. Stage new archive: `git add 2023/<verifier>.zip`
8. Commit: `git commit -m <msg>`, where `<msg>` is a relevant message
9. Push: `git push origin <branch>`
10. Open MR on GitLab with created `<branch>`

## Troubleshooting failed Ci "check-archives-with-service"

Please refer to the [wiki entry about this topic](https://gitlab.com/sosy-lab/sv-comp/archives-2023/-/wikis/FAQ-CI-with-CoVeriTeam-Service)
